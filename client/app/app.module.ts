import { NgModule }                   from '@angular/core';
import { BrowserModule }              from '@angular/platform-browser';
import { HttpModule }                 from '@angular/http';
import { AngularFireModule }          from 'angularfire2';
import { AngularFireDatabaseModule }  from 'angularfire2/database';
import { AngularFireAuth }            from 'angularfire2/auth';

//COMPONENTS
import { AppComponent }             from './app.component';
import { HeaderComponent }          from '../components/header/header.component'
import { FooterComponent }          from '../components/footer/footer.component';
import { HomeComponent }            from '../components/home/home.component';
import { AboutComponent }           from '../components/about/about.component';
import { ServicesComponent }        from '../components/services/services.component';
import { PortfolioComponent }       from '../components/portfolio/portfolio.component';
import { PortfolioDetailComponent } from '../components/portfolio-detail/portfolio-detail.component';
import { LoginComponent }           from '../components/login/login.component';
import { BloodComponent }           from '../components/blood/blood.component';
import { BloodRequestComponent }   from '../components/blood/blood-request/blood-request.component';
import { BloodResponseComponent }   from '../components/blood/blood-response/blood-response.component';
import { WarningComponent }           from '../components/warning/warning.component';

import { SharedModule } from '../shared/shared.module';
import { BrowserAnimationsModule} from '@angular/platform-browser/animations'
import { NoopAnimationsModule} from '@angular/platform-browser/animations'
import { MatFormFieldModule, MatNativeDateModule} from '@angular/material';
import { MatInputModule } from '@angular/material';
import { MatOptionModule } from '@angular/material';
import { MatSelectModule } from '@angular/material';
import { MatDatepickerModule } from '@angular/material';
import { MatButtonModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// ROUTING
import { AppRoutingModule } from './app-routing.module';

// SERVICES
import { HttpService }    from '../services/http.service';
import { DataService }    from '../services/data.service';
import { ConfigService }  from '../services/config.service';
import { AuthService }    from "../services/auth.service";

//Dashboard
import { ScriptLoaderService }  from "../services/script-loader.service";
import { GlobalErrorHandler }   from "../services/error-handler.service";

import { NgSpinningPreloader } from 'ng2-spinning-preloader';

const config = {
  apiKey: "AIzaSyC7C1pbhG8pxwfj26b5wm0Bvl4Ca2Eai6A",
  authDomain: "localhost",
  databaseURL: "https://auth-app-bdf47.firebaseio.com",
  storageBucket: "stackblitzfire.appspot.com",
  messagingSenderId: "584479586760"
};

@NgModule({
  imports: [
    BrowserModule,
    HttpModule,
    AppRoutingModule,
    SharedModule,
    AngularFireModule.initializeApp(config),
    AngularFireDatabaseModule,
    BrowserAnimationsModule,
    NoopAnimationsModule,
    MatFormFieldModule,
    MatInputModule,
    MatOptionModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatButtonModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    AppComponent,
    AboutComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    ServicesComponent,
    PortfolioComponent,
    PortfolioDetailComponent,
    LoginComponent,
    BloodComponent,
    BloodRequestComponent,
    BloodResponseComponent,
    WarningComponent
  ],
  providers: [
    HttpService,
    DataService,
    ConfigService,
    AuthService,
    NgSpinningPreloader,
    ScriptLoaderService,
    GlobalErrorHandler,
    AngularFireAuth
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
