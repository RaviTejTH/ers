import { NgModule } from '@angular/core';
import { Routes, RouterModule } from  '@angular/router';

// COMPONENTS
import { NotFoundComponent }        from '../shared/components/not-found/not-found.component';
import { HeaderComponent }          from '../components/header/header.component'
import { FooterComponent }          from '../components/footer/footer.component';
import { HomeComponent }            from '../components/home/home.component';
import { AboutComponent }           from '../components/about/about.component';
import { ServicesComponent }        from '../components/services/services.component';
import { PortfolioComponent }       from '../components/portfolio/portfolio.component';
import { PortfolioDetailComponent } from '../components/portfolio-detail/portfolio-detail.component';
import { LoginComponent }           from '../components/login/login.component';
import { BloodComponent }           from '../components/blood/blood.component';
import { BloodResponseComponent }   from '../components/blood/blood-response/blood-response.component';
import { WarningComponent }           from '../components/warning/warning.component';


// IMPORT ROUTES
export const routes: Routes = [
    {
        path: '',
        children: [{
            path: '',
            component: HomeComponent
        }, {
            path: '',
            component: FooterComponent,
            outlet: 'Footer',
        }]
    },
    {
        path: 'about',
        children: [
            {
                path: '',
                component: HeaderComponent,
                outlet: 'Header',
            }, {
                path: '',
                component: AboutComponent
            }, {
                path: '',
                component: FooterComponent,
                outlet: 'Footer',
            }]
    },
    {
        path: 'blood',
        children: [
            {
                path: '',
                component: HeaderComponent,
                outlet: 'Header',
            }, {
                path: '',
                component: BloodComponent
            }, {
                path: '',
                component: FooterComponent,
                outlet: 'Footer',
            }]
    },{
        path: 'blood-response',
        children: [
            {
                path: '',
                component: HeaderComponent,
                outlet: 'Header',
            }, {
                path: '',
                component: BloodResponseComponent
            }, {
                path: '',
                component: FooterComponent,
                outlet: 'Footer',
            }]
    },{
        path: 'early-warning',
        children: [
            {
                path: '',
                component: HeaderComponent,
                outlet: 'Header',
            }, {
                path: '',
                component: WarningComponent
            }, {
                path: '',
                component: FooterComponent,
                outlet: 'Footer',
            }]
    },
    {
        path: 'portfolio',
        children: [
            {
                path: '',
                component: HeaderComponent,
                outlet: 'Header',
            }, {
                path: '',
                component: PortfolioComponent
            }, {
                path: '',
                component: FooterComponent,
                outlet: 'Footer',
            }]
    },
    {
        path: 'portfolio/:id',
        children: [
            {
                path: '',
                component: HeaderComponent,
                outlet: 'Header',
            }, {
                path: '',
                component: PortfolioDetailComponent
            }, {
                path: '',
                component: FooterComponent,
                outlet: 'Footer',
            }]
    },
    {
        path: 'login',
        children: [
            {
                path: '',
                component: HeaderComponent,
                outlet: 'Header',
            }, {
                path: '',
                component: LoginComponent
            }, {
                path: '',
                component: FooterComponent,
                outlet: 'Footer',
            }]
    },
  {
   path: '404',
   component: NotFoundComponent
  },
  {
    path: '**',
    redirectTo: '/404'
  }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})

export class AppRoutingModule {}
