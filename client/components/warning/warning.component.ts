import { Component } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { HttpService } from '../../services/http.service';

@Component({
  selector: 'warning',
  styleUrls: ['warning.scss'],
  templateUrl: 'warning.pug'
})

export class WarningComponent {

dataCapture: any;

constructor(private router: Router, private http : HttpService){
}

ngOnInit() {
}

saveBasicDetails(){

    this.dataCapture = {    }

    let url = 'http://httpbin.org/post';
    this.http.post(url,this.dataCapture).subscribe((data: any) => {
      let response = JSON.parse(data._body);
      });
}

}
