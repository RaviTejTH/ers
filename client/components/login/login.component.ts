import { Component } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { HttpService } from '../../services/http.service';

@Component({
  selector: 'login',
  styleUrls: ['login.scss'],
  templateUrl: 'login.pug'
})

export class LoginComponent {

dataCapture: any;
name: any;
phone: any;
dob: any;
gender: any;
blood: any;
submit: any;
secret_code =  1234;
otp: any;

constructor(private router: Router, private http : HttpService){
}

ngOnInit() {
}

saveBasicDetails(){

    this.dataCapture = {
      "name": this.name,
      'phone': this.phone,
      'dob': this.dob,
      'gender': this.gender,
      'blood': this.blood
    }
    sessionStorage.setItem('basicDetails', JSON.stringify(this.dataCapture));
    this.submit = true;
    this.router.navigate(['/']);
    let url = 'http://httpbin.org/post';
    this.http.post(url,this.dataCapture).subscribe((data: any) => {
      let response = JSON.parse(data._body);
      });
}

}
